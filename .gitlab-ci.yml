image:
   name: "python:3.7"

stages:
  - build
  - test
  - doc

# Change pip's cache directory to be inside the project directory since we can
# only cache local items.
variables:
    PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"
    PYTHONTOOLS_VENV_PATH: "$CI_PROJECT_DIR/venv/qlknn"
    PYTHONTOOLS_DIR: "$CI_PROJECT_DIR"
    PYTEST_MARK: "not psql and not fails_on_ci"
    PACKAGE: "qlknn"

# Pip's cache doesn't store the python packages
# https://pip.pypa.io/en/stable/reference/pip_install/#caching
#
# If you want to also cache the installed packages, you have to install
# them in a virtualenv and cache it as well.
.cache_python: &cache_python
  paths:
    - $PIP_CACHE_DIR


########################################
# General Docker environment templates #
########################################

##############################
# Global tests and templates #
##############################
before_script:
  # In standalone local Docker also source 00_prepare_venv_path.sh
  - source envs/docker-python/20_setenv_pure_python.sh
  - source envs/docker-python/21_activate_python_venv.sh
  - source envs/docker-python/22_install_qualikiz_tools.sh
  - echo "*:*:*:testuser:testpassword" > ~/.pgpass

.collect_deps_manually: &collect_deps_manually |
  export BUILD_DEPS=$(cat pyproject.toml | grep requires | cut -d'=' -f2- | tr -d ,\"\' | sed "s/^ \[//" | sed "s/\]$//")
  export RUN_DEP_FILES=$(find requirements_* -maxdepth 1)

.print_debugging: &print_debugging |
  python --version # Print out python version for debugging
  pip --version # Show pip version for debugging
  echo $PYTHONPATH
  echo $PATH
  echo BUILD_DEPS=$BUILD_DEPS
  echo RUN_DEP_FILES=$RUN_DEP_FILES

# venv editable install
.install_venv_editable_template:
  script:
    - *collect_deps_manually
    - *print_debugging
      # Now we install dependencies manually
    - pip install --use-feature=2020-resolver --upgrade $BUILD_DEPS # Install build deps manually
    - python setup.py --version # Print setuptools found version
    - python -m pip freeze > $PYTHONTOOLS_DIR/pre_install_packages.txt
    - for file in $RUN_DEP_FILES; do echo Installing $file; pip install -r $file; done; # Install run deps manually
      # Install from wheel
      # Should install almost nothing, as we installed most of it ourselves
      # Install without extras here. We installed these manually.
    - pip install --use-feature=2020-resolver --no-build-isolation --upgrade --editable . # Install local folder in editable mode
    - pip freeze > $PYTHONTOOLS_DIR/post_install_packages.txt
      # Do basic sanity checking
    - mkdir tmp && cd tmp
    - cat $PYTHONTOOLS_DIR/$PACKAGE/version.py # Check if version file was generated
    - python -c "import $PACKAGE; print($PACKAGE.__version__); print($PACKAGE.__path__)" # Try regular import
      # Run the real test, pytest
    - which pytest
    - pytest --version
    - echo Running "pytest --cov=$PACKAGE --cov-report=term --cov-report=xml:$PYTHONTOOLS_DIR/coverage.xml --junit-xml=$PYTHONTOOLS_DIR/junit.xml --ignore=../tests/NNDB -m '"$PYTEST_MARK"' '"$PYTHONTOOLS_DIR"'"
    - pytest --cov=$PACKAGE --cov-report=term --cov-report=xml:$PYTHONTOOLS_DIR/coverage.xml --junit-xml=$PYTHONTOOLS_DIR/junit.xml --ignore=../tests/NNDB -m "$PYTEST_MARK" "$PYTHONTOOLS_DIR"
  # One pipeline has one artifact. We need both the test report _and_ the installed packages to pass on
  artifacts:
    name: "$CI_JOB_NAME-$CI_COMMIT_REF_NAME"
    reports:
      junit: [$PYTHONTOOLS_DIR/junit.xml]
      cobertura: [$PYTHONTOOLS_DIR/coverage.xml]
    when: always
    expire_in: 1 day # These are only used in quick interactive CI debugging, and in next jobs
  stage: test
  cache:
    <<: *cache_python
    policy: push
  needs: [] # Only needs a sane python env

# editable venv-like install
install_venv_editable:
  extends: .install_venv_editable_template

install_venv_python36:
  image: python:3.6
  extends: .install_venv_editable_template
  except:
    - documentation

install_venv_python38:
  image: python:3.8
  extends: .install_venv_editable_template
  except:
    - documentation

# Drop support for Python 3.9. No tensorflow wheel available
.install_venv_python39:
  image: python:3.9
  extends: .install_venv_editable_template
  except:
    - documentation

# Drop support for Python 3.9. No tensorflow wheel available
.install_venv_python310:
  image: python:3.10-rc
  extends: .install_venv_editable_template
  except:
    - documentation

flake8:
  stage: test
  before_script:
    - pip install flake8 pyflakes anybadge
  script:
    - mkdir -p ./flake8
    - flake8 --exit-zero --doctests --statistics --count $(find $PACKAGE -name '*.py') | tee flake8.txt
    - PEP8_VIOLATIONS=$(tail flake8.txt -n1)
    - echo "Flake8 finds $PEP8_VIOLATIONS PEP8 violations"
    - anybadge -ou --label=flake8 --value=$PEP8_VIOLATIONS --file=flake8/flake8.svg -c silver
  artifacts:
    paths:
      - ./flake8/

pylint:
  stage: test
  before_script:
    - pip install pylint pylint-exit anybadge
  script:
    - mkdir -p ./pylint
    - pylint --rcfile=.pylintrc --output-format=text $PACKAGE | tee ./pylint/pylint.log || pylint-exit $?
    - PYLINT_SCORE=$(sed -n 's/^Your code has been rated at \([-0-9.]*\)\/.*/\1/p' ./pylint/pylint.log)
    - echo "Pylint score is $PYLINT_SCORE"
    - anybadge -ou --label=pylint --value=$PYLINT_SCORE --file=pylint/pylint.svg 2=red 4=orange 8=yellow 10=green
  artifacts:
    paths:
      - ./pylint/

black:
  stage: test
  script:
    - pip install black
    - black --diff --color $PACKAGE

pages:
  stage: doc
  script:
    - apt-get update && apt-get install -y python3-sphinx python3-ipython python3-recommonmark
    - pip install --upgrade --editable .[docs]
    - make docs
    - mv docs/_build/html/ public/
  artifacts:
    paths:
      - public/
      - docs/_build/
      - docs/source/generated/
  only:
      - master
      - documentation
