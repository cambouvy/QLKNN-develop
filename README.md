# QLKNN

*A collection of tools to create QuaLiKiz Neural Networks.*

Read the [GitLab Pages](https://karel-van-de-plassche.gitlab.io/QLKNN-develop/) and [wiki](https://gitlab.com/Karel-van-de-Plassche/QLKNN-develop/-/wikis/home) for more information.

## FAQ
* How do I run the unit tests?

        python setup.py test
