import json
from qlknn.training import train_NDNN
import os

if __name__ == "__main__":

    # Value to test the L2 parameter for
    value_to_search_L2 = 0.00005

    # Value to test the batch size parameter for
    value_to_search_bs = 100

    # Gives the NN id
    counter = 10

    # Train one NN for each L2 param value and save it in the networks folder
    with open("./settings.json", "r+") as jsonFile:
        data = json.load(jsonFile)
        data["cost_l2_scale"] = value_to_search_L2
        data["minibatches"] = value_to_search_bs
        jsonFile.seek(0)  # rewind
        json.dump(data, jsonFile, indent=2)
        jsonFile.truncate()

    train_NDNN.train_NDNN_from_folder2(counter)



