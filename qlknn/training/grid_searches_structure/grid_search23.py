import json
from qlknn.training import train_NDNN
import os

if __name__ == "__main__":

    # Different values to test the number of layers parameter for
    values_to_search_layers = [5]

    # Different values to test the number of neurons parameter for
    values_to_search_neurons = [128]

    # Gives the NN id
    counter = 23

    # Train one NN for each L2 param value and save it in the networks folder
    for i in values_to_search_layers:
        for j in values_to_search_neurons:
            with open("./settings.json", "r+") as jsonFile:
                data = json.load(jsonFile)
                hidden_neurons = []
                hidden_activation = []
                for layer in range(i):
                    hidden_neurons.append(j)
                    hidden_activation.append("tanh")
                data["hidden_neurons"] = hidden_neurons
                data["hidden_activation"] = hidden_activation
                jsonFile.seek(0)  # rewind
                json.dump(data, jsonFile, indent=2)
                jsonFile.truncate()
            #counter += 1

            train_NDNN.train_NDNN_from_folder2(counter)



